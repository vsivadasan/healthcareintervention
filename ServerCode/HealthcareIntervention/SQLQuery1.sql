﻿CREATE TABLE [dbo].[Messages] (
    [MessageID]   INT            IDENTITY (1, 1) NOT NULL,
    [SendTime]    DATETIME       NOT NULL,
    [Read]        BIT            NOT NULL,
    [Content]     NVARCHAR (MAX) NULL,
    [Receiver_Id] NVARCHAR (128) NULL,
    [Sender_Id]   NVARCHAR (128) NULL,
    CONSTRAINT [PK_dbo.Messages] PRIMARY KEY CLUSTERED ([MessageID] ASC),
    CONSTRAINT [FK_dbo.Messages_dbo.AspNetUsers_Receiver_Id] FOREIGN KEY ([Receiver_Id]) REFERENCES [dbo].[AspNetUsers] ([Id]),
    CONSTRAINT [FK_dbo.Messages_dbo.AspNetUsers_Sender_Id] FOREIGN KEY ([Sender_Id]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Receiver_Id]
    ON [dbo].[Messages]([Receiver_Id] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Sender_Id]
    ON [dbo].[Messages]([Sender_Id] ASC);

