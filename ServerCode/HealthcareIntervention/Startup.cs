﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using System;

[assembly: OwinStartupAttribute(typeof(HealthcareIntervention.Startup))]
namespace HealthcareIntervention
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");
            app.UseHangfireDashboard();
            app.UseHangfireServer();
            int interval = 5;
            RecurringJob.AddOrUpdate(() => HealthcareIntervention.Controllers.BlastResponseController.LoadBlasts(), Cron.MinuteInterval(interval));
        }
    }
}
