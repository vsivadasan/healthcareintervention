namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForumThreadChange1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ForumThreads", "ThreadName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ForumThreads", "ThreadName", c => c.String());
        }
    }
}
