namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class study11 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AspNetUsers", name: "Study_StudyId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.AspNetUsers", name: "Study_StudyId1", newName: "Study_StudyId");
            RenameColumn(table: "dbo.AspNetUsers", name: "__mig_tmp__0", newName: "Study_StudyId1");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Study_StudyId1", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Study_StudyId", newName: "IX_Study_StudyId1");
            RenameIndex(table: "dbo.AspNetUsers", name: "__mig_tmp__0", newName: "IX_Study_StudyId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Study_StudyId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Study_StudyId1", newName: "IX_Study_StudyId");
            RenameIndex(table: "dbo.AspNetUsers", name: "__mig_tmp__0", newName: "IX_Study_StudyId1");
            RenameColumn(table: "dbo.AspNetUsers", name: "Study_StudyId1", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.AspNetUsers", name: "Study_StudyId", newName: "Study_StudyId1");
            RenameColumn(table: "dbo.AspNetUsers", name: "__mig_tmp__0", newName: "Study_StudyId");
        }
    }
}
