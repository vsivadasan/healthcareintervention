namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlastSchedule1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastSchedules",
                c => new
                    {
                        BlastScheduleId = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        AnnouncementMessageId = c.Int(nullable: false),
                        BlastType = c.String(),
                        SendTime = c.DateTime(nullable: false),
                        StudyId = c.Int(nullable: false),
                        Visible = c.Boolean(nullable: false),
                        Responded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BlastScheduleId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlastSchedules");
        }
    }
}
