namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blast_textwithresponse : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastTemplateTextWithResponses",
                c => new
                    {
                        BlastTemplateTextWithResponseId = c.Int(nullable: false, identity: true),
                        BlastMessage = c.String(),
                        AnnouncementMessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BlastTemplateTextWithResponseId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlastTemplateTextWithResponses");
        }
    }
}
