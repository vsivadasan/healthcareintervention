namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlastOptionsQuestion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastTemplateOptionsQuestions",
                c => new
                    {
                        BlastTemplateOptionsQuestionId = c.Int(nullable: false, identity: true),
                        Question = c.String(nullable: false),
                        AnnouncementMessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BlastTemplateOptionsQuestionId);
            
            AlterColumn("dbo.BlastTemplateOptions", "Option", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BlastTemplateOptions", "Option", c => c.String());
            DropTable("dbo.BlastTemplateOptionsQuestions");
        }
    }
}
