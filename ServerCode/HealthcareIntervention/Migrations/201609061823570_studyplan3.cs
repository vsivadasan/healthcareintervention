namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class studyplan3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnnouncementMessages", "AnnouncementName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AnnouncementMessages", "AnnouncementName");
        }
    }
}
