namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class avatar2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Avatar_AvatarID", "dbo.Avatars");
            DropIndex("dbo.AspNetUsers", new[] { "Avatar_AvatarID" });
            RenameColumn(table: "dbo.AspNetUsers", name: "Avatar_AvatarID", newName: "Avatar_Id");
            DropPrimaryKey("dbo.Avatars");
            AddColumn("dbo.Avatars", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.AspNetUsers", "Avatar_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Avatars", "AvatarID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Avatars", "Id");
            CreateIndex("dbo.AspNetUsers", "Avatar_Id");
            AddForeignKey("dbo.AspNetUsers", "Avatar_Id", "dbo.Avatars", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Avatar_Id", "dbo.Avatars");
            DropIndex("dbo.AspNetUsers", new[] { "Avatar_Id" });
            DropPrimaryKey("dbo.Avatars");
            AlterColumn("dbo.Avatars", "AvatarID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.AspNetUsers", "Avatar_Id", c => c.Int());
            DropColumn("dbo.Avatars", "Id");
            AddPrimaryKey("dbo.Avatars", "AvatarID");
            RenameColumn(table: "dbo.AspNetUsers", name: "Avatar_Id", newName: "Avatar_AvatarID");
            CreateIndex("dbo.AspNetUsers", "Avatar_AvatarID");
            AddForeignKey("dbo.AspNetUsers", "Avatar_AvatarID", "dbo.Avatars", "AvatarID");
        }
    }
}
