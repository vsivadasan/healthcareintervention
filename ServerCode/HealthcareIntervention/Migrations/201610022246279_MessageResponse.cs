namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MessageResponse : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastResponses",
                c => new
                    {
                        BlastResponseId = c.Int(nullable: false, identity: true),
                        BlastScheduleId = c.Int(nullable: false),
                        Response = c.String(),
                        UserId = c.String(),
                        ReadByNurse = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.BlastResponseId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlastResponses");
        }
    }
}
