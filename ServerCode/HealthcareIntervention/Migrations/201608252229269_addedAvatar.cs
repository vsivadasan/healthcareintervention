namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAvatar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Avatars",
                c => new
                    {
                        AvatarID = c.Int(nullable: false, identity: true),
                        Image = c.Binary(storeType: "image"),
                    })
                .PrimaryKey(t => t.AvatarID);
            
            AddColumn("dbo.AspNetUsers", "Avatar_AvatarID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Avatar_AvatarID");
            AddForeignKey("dbo.AspNetUsers", "Avatar_AvatarID", "dbo.Avatars", "AvatarID");
            DropColumn("dbo.AspNetUsers", "AvatarID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "AvatarID", c => c.Int(nullable: false));
            DropForeignKey("dbo.AspNetUsers", "Avatar_AvatarID", "dbo.Avatars");
            DropIndex("dbo.AspNetUsers", new[] { "Avatar_AvatarID" });
            DropColumn("dbo.AspNetUsers", "Avatar_AvatarID");
            DropTable("dbo.Avatars");
        }
    }
}
