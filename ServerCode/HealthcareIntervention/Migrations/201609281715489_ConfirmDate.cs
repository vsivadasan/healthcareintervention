namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ConfirmDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PatientStudyMappings", "ConfirmedStartDate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PatientStudyMappings", "ConfirmedStartDate");
        }
    }
}
