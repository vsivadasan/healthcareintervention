// <auto-generated />
namespace HealthcareIntervention.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class study13 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(study13));
        
        string IMigrationMetadata.Id
        {
            get { return "201609042321441_study1.3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
