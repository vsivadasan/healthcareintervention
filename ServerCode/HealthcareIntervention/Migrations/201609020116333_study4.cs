namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class study4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnnouncementDays",
                c => new
                    {
                        AnnouncementDayId = c.Int(nullable: false, identity: true),
                        DayNo = c.Int(nullable: false),
                        StudyPlan_StudyId = c.Int(),
                    })
                .PrimaryKey(t => t.AnnouncementDayId)
                .ForeignKey("dbo.StudyPlans", t => t.StudyPlan_StudyId)
                .Index(t => t.StudyPlan_StudyId);
            
            CreateTable(
                "dbo.AnnouncementMessages",
                c => new
                    {
                        AnnouncementMessageId = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        BlastMessage = c.String(),
                        AnnouncementDay_AnnouncementDayId = c.Int(),
                    })
                .PrimaryKey(t => t.AnnouncementMessageId)
                .ForeignKey("dbo.AnnouncementDays", t => t.AnnouncementDay_AnnouncementDayId)
                .Index(t => t.AnnouncementDay_AnnouncementDayId);
            
            CreateTable(
                "dbo.StudyPlans",
                c => new
                    {
                        StudyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudyId)
                .ForeignKey("dbo.Studies", t => t.StudyId)
                .Index(t => t.StudyId);
            
            CreateTable(
                "dbo.Studies",
                c => new
                    {
                        StudyId = c.Int(nullable: false, identity: true),
                        StudyName = c.String(),
                        Form = c.Boolean(nullable: false),
                        Messages = c.Boolean(nullable: false),
                        Announcements = c.Boolean(nullable: false),
                        Capacity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudyId);
            
            AddColumn("dbo.AspNetUsers", "Study_StudyId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Study_StudyId1", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Study_StudyId");
            CreateIndex("dbo.AspNetUsers", "Study_StudyId1");
            AddForeignKey("dbo.AspNetUsers", "Study_StudyId", "dbo.Studies", "StudyId");
            AddForeignKey("dbo.AspNetUsers", "Study_StudyId1", "dbo.Studies", "StudyId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudyPlans", "StudyId", "dbo.Studies");
            DropForeignKey("dbo.AspNetUsers", "Study_StudyId1", "dbo.Studies");
            DropForeignKey("dbo.AspNetUsers", "Study_StudyId", "dbo.Studies");
            DropForeignKey("dbo.AnnouncementDays", "StudyPlan_StudyId", "dbo.StudyPlans");
            DropForeignKey("dbo.AnnouncementMessages", "AnnouncementDay_AnnouncementDayId", "dbo.AnnouncementDays");
            DropIndex("dbo.AspNetUsers", new[] { "Study_StudyId1" });
            DropIndex("dbo.AspNetUsers", new[] { "Study_StudyId" });
            DropIndex("dbo.StudyPlans", new[] { "StudyId" });
            DropIndex("dbo.AnnouncementMessages", new[] { "AnnouncementDay_AnnouncementDayId" });
            DropIndex("dbo.AnnouncementDays", new[] { "StudyPlan_StudyId" });
            DropColumn("dbo.AspNetUsers", "Study_StudyId1");
            DropColumn("dbo.AspNetUsers", "Study_StudyId");
            DropTable("dbo.Studies");
            DropTable("dbo.StudyPlans");
            DropTable("dbo.AnnouncementMessages");
            DropTable("dbo.AnnouncementDays");
        }
    }
}
