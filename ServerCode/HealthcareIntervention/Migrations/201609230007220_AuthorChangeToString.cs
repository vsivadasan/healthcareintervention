namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuthorChangeToString : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ForumPosts", "Author_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ForumPosts", new[] { "Author_Id" });
            AddColumn("dbo.ForumPosts", "Author", c => c.String());
            DropColumn("dbo.ForumPosts", "Author_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ForumPosts", "Author_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.ForumPosts", "Author");
            CreateIndex("dbo.ForumPosts", "Author_Id");
            AddForeignKey("dbo.ForumPosts", "Author_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
