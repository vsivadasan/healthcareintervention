namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedBlastType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AnnouncementMessages", "BlastType", c => c.String());
            DropColumn("dbo.AnnouncementMessages", "BlastMessage");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AnnouncementMessages", "BlastMessage", c => c.String());
            DropColumn("dbo.AnnouncementMessages", "BlastType");
        }
    }
}
