namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blast_textonly_create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastTemplateTextOnlies",
                c => new
                    {
                        BlastTemplateTextOnlyId = c.Int(nullable: false, identity: true),
                        BlastMessage = c.String(),
                        AnnouncementMessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BlastTemplateTextOnlyId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlastTemplateTextOnlies");
        }
    }
}
