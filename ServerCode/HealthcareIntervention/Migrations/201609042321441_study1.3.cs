namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class study13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Studies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Study_StudyId", "dbo.Studies");
            DropForeignKey("dbo.AspNetUsers", "Study_StudyId1", "dbo.Studies");
            DropIndex("dbo.Studies", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Study_StudyId" });
            DropIndex("dbo.AspNetUsers", new[] { "Study_StudyId1" });
            CreateTable(
                "dbo.NurseStudyMappings",
                c => new
                    {
                        NurseStudyId = c.Int(nullable: false, identity: true),
                        NurseId = c.String(),
                        StudyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NurseStudyId);
            
            DropColumn("dbo.Studies", "ApplicationUser_Id");
            DropColumn("dbo.AspNetUsers", "Study_StudyId");
            DropColumn("dbo.AspNetUsers", "Study_StudyId1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Study_StudyId1", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Study_StudyId", c => c.Int());
            AddColumn("dbo.Studies", "ApplicationUser_Id", c => c.String(maxLength: 128));
            DropTable("dbo.NurseStudyMappings");
            CreateIndex("dbo.AspNetUsers", "Study_StudyId1");
            CreateIndex("dbo.AspNetUsers", "Study_StudyId");
            CreateIndex("dbo.Studies", "ApplicationUser_Id");
            AddForeignKey("dbo.AspNetUsers", "Study_StudyId1", "dbo.Studies", "StudyId");
            AddForeignKey("dbo.AspNetUsers", "Study_StudyId", "dbo.Studies", "StudyId");
            AddForeignKey("dbo.Studies", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
