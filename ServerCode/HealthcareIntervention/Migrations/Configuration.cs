namespace HealthcareIntervention.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HealthcareIntervention.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        bool AddUserAndRole(HealthcareIntervention.Models.ApplicationDbContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<IdentityRole>
                (new RoleStore<IdentityRole>(context));
            ir = rm.Create(new IdentityRole("Admin"));
            ir = rm.Create(new IdentityRole("Nurse"));
            ir = rm.Create(new IdentityRole("Volunteer"));
            var um = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            DateTime dateValue;
            DateTime.TryParse("08/22/1991", out dateValue);
            var user = new ApplicationUser()
            {
                FirstName="Vishnu",
                LastName="Sivadasan",
                Gender="Male",
                DateOfBirth=dateValue,
                //AvatarID=0,
                UserName = "vsivadas@cse.unl.edu",
                EmailConfirmed = true,
                Email = "vsivadas@cse.unl.edu"

            };
            ir = um.Create(user, "Vishnu@123");
            if (ir.Succeeded == false)
                return ir.Succeeded;
            ir = um.AddToRole(user.Id, "Admin");
            return ir.Succeeded;

           
        }
        protected override void Seed(HealthcareIntervention.Models.ApplicationDbContext context)
        {
            AddUserAndRole(context);

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
           
        }
    }
}
