// <auto-generated />
namespace HealthcareIntervention.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Blast_template : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Blast_template));
        
        string IMigrationMetadata.Id
        {
            get { return "201609271741445_Blast_template"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
