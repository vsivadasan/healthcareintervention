namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dateTimetoResponse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlastResponses", "TimeRead", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlastResponses", "TimeRead");
        }
    }
}
