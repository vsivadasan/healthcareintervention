namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Study10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientStudyMappings",
                c => new
                    {
                        PatientStudyId = c.Int(nullable: false, identity: true),
                        PatientId = c.String(),
                        StudyId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PatientStudyId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PatientStudyMappings");
        }
    }
}
