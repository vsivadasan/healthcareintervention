namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class avatarUserId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Avatars", "userId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Avatars", "userId");
        }
    }
}
