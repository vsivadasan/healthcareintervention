namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedForum : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForumPosts",
                c => new
                    {
                        ForumPostId = c.Int(nullable: false, identity: true),
                        PostingTime = c.DateTime(nullable: false),
                        Content = c.String(),
                        Author_Id = c.String(maxLength: 128),
                        ForumThread_ForumThreadId = c.Int(),
                    })
                .PrimaryKey(t => t.ForumPostId)
                .ForeignKey("dbo.AspNetUsers", t => t.Author_Id)
                .ForeignKey("dbo.ForumThreads", t => t.ForumThread_ForumThreadId)
                .Index(t => t.Author_Id)
                .Index(t => t.ForumThread_ForumThreadId);
            
            CreateTable(
                "dbo.Fora",
                c => new
                    {
                        StudyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudyId)
                .ForeignKey("dbo.Studies", t => t.StudyId)
                .Index(t => t.StudyId);
            
            CreateTable(
                "dbo.ForumThreads",
                c => new
                    {
                        ForumThreadId = c.Int(nullable: false, identity: true),
                        ThreadName = c.String(),
                        Forum_StudyId = c.Int(),
                    })
                .PrimaryKey(t => t.ForumThreadId)
                .ForeignKey("dbo.Fora", t => t.Forum_StudyId)
                .Index(t => t.Forum_StudyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ForumThreads", "Forum_StudyId", "dbo.Fora");
            DropForeignKey("dbo.ForumPosts", "ForumThread_ForumThreadId", "dbo.ForumThreads");
            DropForeignKey("dbo.Fora", "StudyId", "dbo.Studies");
            DropForeignKey("dbo.ForumPosts", "Author_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ForumThreads", new[] { "Forum_StudyId" });
            DropIndex("dbo.Fora", new[] { "StudyId" });
            DropIndex("dbo.ForumPosts", new[] { "ForumThread_ForumThreadId" });
            DropIndex("dbo.ForumPosts", new[] { "Author_Id" });
            DropTable("dbo.ForumThreads");
            DropTable("dbo.Fora");
            DropTable("dbo.ForumPosts");
        }
    }
}
