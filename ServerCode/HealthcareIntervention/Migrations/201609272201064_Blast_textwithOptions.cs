namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Blast_textwithOptions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlastTemplateOptions",
                c => new
                    {
                        BlastTemplateOptionsId = c.Int(nullable: false, identity: true),
                        Option = c.String(),
                        AnnouncementMessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BlastTemplateOptionsId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BlastTemplateOptions");
        }
    }
}
