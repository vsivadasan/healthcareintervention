namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class study8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Studies", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Studies", "ApplicationUser_Id");
            AddForeignKey("dbo.Studies", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Studies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Studies", new[] { "ApplicationUser_Id" });
            DropColumn("dbo.Studies", "ApplicationUser_Id");
        }
    }
}
