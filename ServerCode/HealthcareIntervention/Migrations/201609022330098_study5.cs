namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class study5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Studies", "Forum", c => c.Boolean(nullable: false));
            DropColumn("dbo.Studies", "Form");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Studies", "Form", c => c.Boolean(nullable: false));
            DropColumn("dbo.Studies", "Forum");
        }
    }
}
