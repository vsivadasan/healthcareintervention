namespace HealthcareIntervention.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class studyIdinNurse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlastResponses", "StudyId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlastResponses", "StudyId");
        }
    }
}
