﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class StudyPlansController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        ///GET : Get or edit Study Plan
        public ActionResult GetStudyPlan()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId= Convert.ToInt32(Session["StudyId"].ToString());
            Study study = db.Studies.Find(StudyId);            
            StudyPlan plan;
            if(study.StudyPlan!=null)
            {
                plan = study.StudyPlan;
            }
            else//Study plan is not present
            {//Create a study plan
                plan = new StudyPlan();
                plan.Study = study;
                plan.StudyId = study.StudyId;
                plan.AnnouncementDays = new List<AnnouncementDay>();
                db.StudyPlans.Add(plan);
                db.SaveChanges();
                study.StudyPlan = plan;
                db.Entry(study).State = EntityState.Modified;
                db.SaveChanges();
            }
            StudyPlanViewModel model =new StudyPlanViewModel();
            model.Days = new List<DayAndEvent>();
            model.Study = study;
            model.StudyId = study.StudyId;
            foreach(AnnouncementDay day in plan.AnnouncementDays.OrderBy(x=>x.DayNo))
            {
                DayAndEvent dayAndEvent = new DayAndEvent();
                dayAndEvent.Day = day.DayNo;
                dayAndEvent.Events = new List<Event>();
                foreach(AnnouncementMessage Event1 in day.AnnouncementMessages)
                {
                    Event e1 = new Models.Event();
                    e1.AnnouncementId = Event1.AnnouncementMessageId;
                    e1.EventName = Event1.AnnouncementName;
                    switch (Event1.BlastType)
                    {
                        case "TextOnly":
                            e1.EventType = "Text only";
                            break;
                        case "TextWithResponse":
                            e1.EventType = "Text with response";
                            break;
                        case "ChooseOneFromList":
                            e1.EventType = "Pick one from a list";
                            break;
                        case "ChooseSeveralFromList":
                            e1.EventType = "Pick Several from a list";
                            break;
                        default:
                            e1.EventType = "Unknown";
                            break;
                    }
                    
                    e1.Time = Event1.Time;
                    dayAndEvent.Events.Add(e1);
                }
                model.Days.Add(dayAndEvent);
            }
            return View(model);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
