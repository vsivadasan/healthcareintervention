﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class MenuController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Menu
        public ActionResult RenderMenu()
        {
            
            int StudyId = 0;
            if ((Session["StudyId"] != null)){
                StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            }
            string MyId = null;
            if ((Session["LoggedUserID"] != null))
            {
                MyId = (Session["LoggedUserID"].ToString());
            }
               
            var navBar = new NavBarModel();
            string template;
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(RoleNames.Admin))
                {
                    template = navBar.AdminNavBar;
                }
                else if (User.IsInRole(RoleNames.Nurse))
                {
                    if (MyId != null)
                    {
                        Session["UnreadMessages"] = db.Messages.Where(x => x.Receiver.Id == MyId).Where(y => y.Read == false).Count();
                    }
                    
                    if (StudyId!=0)
                    {
                        Session["UnreadBlasts"] = db.BlastResponses.Where(x => x.StudyId == StudyId).Where(y => y.ReadByNurse == false).Count();
                    }
                        template = navBar.NurseNavBar;
                }
                else
                {
                    if (MyId != null)
                    {
                        Session["UnreadMessages"] = db.Messages.Where(x => x.Receiver.Id == MyId).Where(y => y.Read == false).Count();
                    }
                    if (StudyId != 0)
                    {
                        Session["UnreadBlasts"] = db.BlastSchedules.Where(x => x.UserId == MyId).Where(y => y.Visible == true).Where(z => z.Responded == false).Count();
                    }
                    template = navBar.VolunteerNavBar;
                }
            }
            else
            {
                template = navBar.LoggedoffBar;
            }


            return PartialView(template);
        }
    }
}