﻿using HealthcareIntervention.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Controllers
{
    public class BlastResponseController : Controller
    {
        private static ApplicationDbContext db = new ApplicationDbContext();

        //GET: Load Blast for Patients
        [Authorize(Roles = RoleNames.Volunteer+ "," + RoleNames.Nurse)]
        public static void LoadBlasts()
        {
            DateTime CurrentTime = DateTime.Now;
            var List = db.BlastSchedules.Where(x => x.Visible == false).Where(y => y.SendTime <= CurrentTime).ToList();
            foreach(var item in List)
            {
                item.Visible = true;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
        // GET: BlastResponse
        [Authorize(Roles = RoleNames.Volunteer)]
        public ActionResult BlastIndexForPatient()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string UserID = (Session["LoggedUserID"].ToString());
            var Blasts = db.BlastSchedules.Where(x => x.UserId == UserID).Where(y => y.StudyId == StudyId).Where(z => z.Visible == true).Where(a => a.Responded == false).ToList();
            if (Blasts.Count == 0)
            {
                return View("EmptyBlastListing");
            }
            else
            {
                List<BlastListing> BlastList = new List<BlastListing>();
                
                foreach (var Blast in Blasts.OrderBy(m=>m.SendTime))
                {
                    BlastListing entry = new BlastListing();
                    entry.BlastScheduleId = Blast.BlastScheduleId;
                    entry.BlastName = db.AnnouncementMessages.Find(Blast.AnnouncementMessageId).AnnouncementName.ToString();
                    BlastList.Add(entry);
                }
                return View("BlastListing",BlastList);
            }
           
        }
        [Authorize(Roles = RoleNames.Nurse)]
        public ActionResult BlastIndexForNurses()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string UserID = (Session["LoggedUserID"].ToString());
            var Patients = db.PatientStudyMappings.Where(x => x.StudyId == StudyId).ToList();
            List<BlastListingNurseEntries> Listing = new List<BlastListingNurseEntries>();
            foreach(var item in Patients)
            {
                BlastListingNurseEntries entry = new BlastListingNurseEntries();
                var Patient = db.Users.Find(item.PatientId);
                entry.UserName = item.PatientId;
                entry.Name = Patient.FirstName + " " + Patient.LastName;
                entry.unread = db.BlastResponses.Where(x => x.ReadByNurse == false).Where(y => y.UserId == item.PatientId).Where(z => z.StudyId == StudyId).Count();
                Listing.Add(entry);
            }
            return View("BlastListingNurse",Listing.OrderByDescending(m=>m.unread));
        }
        [Authorize(Roles = RoleNames.Nurse)]
        public ActionResult ViewResponse(string id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            BlastResponses Response = new BlastResponses();
            Response.Answered = new List<BlastResponseEntry>();
            Response.UnAnswered= new List<BlastResponseEntry>();
            Response.UserId = id;
            var User = db.Users.Find(id);
            ViewBag.Name = User.FirstName + " " + User.LastName;
            var Answered = db.BlastSchedules.Where(x => x.UserId == id).Where(y => y.StudyId == StudyId).Where(z => z.Responded == true).ToList();
            var UnAnswered = db.BlastSchedules.Where(x => x.UserId == id).Where(y => y.StudyId == StudyId).Where(z => z.Responded == false).ToList();

            foreach(var item in Answered)
            {
                BlastResponseEntry MyEntry = new BlastResponseEntry();
                MyEntry.New = false;
                switch (item.BlastType)
                {
                    case "TextOnly":
                        MyEntry.Question = db.BlastTemplateTextOnlies.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().BlastMessage;
                        MyEntry.SendTime = item.SendTime;
                        break;
                    case "TextWithResponse":
                        MyEntry.Question = db.BlastTemplateTextWithResponses.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().BlastMessage;
                        MyEntry.SendTime = item.SendTime;
                        var respons = db.BlastResponses.Where(y => y.BlastScheduleId == item.BlastScheduleId).First();
                        if (respons.ReadByNurse == false)
                        {
                            MyEntry.New = true;
                            respons.ReadByNurse = true;
                            db.Entry(respons).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        MyEntry.Answer = respons.Response;
                        MyEntry.ReadTime = respons.TimeRead;
                        break;
                    case "ChooseOneFromList":
                        MyEntry.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().Question;
                        MyEntry.SendTime = item.SendTime;
                        var respons1= db.BlastResponses.Where(y => y.BlastScheduleId == item.BlastScheduleId).First();
                        if (respons1.ReadByNurse == false)
                        {
                            MyEntry.New = true;
                            respons1.ReadByNurse = true;
                            db.Entry(respons1).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        MyEntry.Answer = respons1.Response;
                        MyEntry.ReadTime = respons1.TimeRead;
                        break;
                    case "ChooseSeveralFromList":
                        MyEntry.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().Question;
                        MyEntry.SendTime = item.SendTime;
                        var respons2 = db.BlastResponses.Where(y => y.BlastScheduleId == item.BlastScheduleId).First();
                        if (respons2.ReadByNurse == false)
                        {
                            MyEntry.New = true;
                            respons2.ReadByNurse = true;
                            db.Entry(respons2).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        MyEntry.Answer = respons2.Response;
                        MyEntry.ReadTime = respons2.TimeRead;
                        break;
                }
                Response.Answered.Add(MyEntry);                  
            }
            foreach (var item in UnAnswered)
            {
                BlastResponseEntry MyEntry = new BlastResponseEntry();
                switch (item.BlastType)
                {
                    case "TextOnly":
                        MyEntry.Question = db.BlastTemplateTextOnlies.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().BlastMessage;
                        MyEntry.SendTime = item.SendTime;
                        break;
                    case "TextWithResponse":
                        MyEntry.Question = db.BlastTemplateTextWithResponses.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().BlastMessage;
                        MyEntry.SendTime = item.SendTime;
                        break;
                    case "ChooseOneFromList":
                        MyEntry.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().Question;
                        MyEntry.SendTime = item.SendTime;
                        break;
                    case "ChooseSeveralFromList":
                        MyEntry.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == item.AnnouncementMessageId).First().Question;
                        MyEntry.SendTime = item.SendTime;
                        break;
                }
                Response.UnAnswered.Add(MyEntry);
            }
            Response.Answered = Response.Answered.OrderByDescending(x => x.SendTime).ToList();
            return View("ViewResponse",Response);
        }

        // GET: BlastResponse/Details/5
        [Authorize(Roles = RoleNames.Volunteer)]
        public ActionResult WriteResponse(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }

            var message = db.BlastSchedules.Find((int)id);
            if (message != null)
            {

                switch (message.BlastType)
                {
                    case "TextOnly":
                        BlastResponseTextOnly item = new BlastResponseTextOnly();
                        item.BlastScheduleId = message.BlastScheduleId;
                        item.Text = db.BlastTemplateTextOnlies.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).First().BlastMessage;
                        return View("TextOnly", item);                       
                    case "TextWithResponse":
                        BlastResponseTextWithResponse item1 = new BlastResponseTextWithResponse();
                        item1.BlastScheduleId = message.BlastScheduleId;
                        item1.Text = db.BlastTemplateTextWithResponses.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).First().BlastMessage;
                        return View("TextWithResponse", item1);
                    case "ChooseOneFromList":
                        BlastResponseChooseOne item2 = new BlastResponseChooseOne();
                        item2.BlastScheduleId = message.BlastScheduleId;
                        item2.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).First().Question;
                        item2.Responses = new List<string>();
                        var options = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).ToList();
                        foreach (var entry in options)
                        {
                            item2.Responses.Add(entry.Option);
                        }
                        return View("ChooseOneOption", item2);

                    case "ChooseSeveralFromList":

                        BlastResponseSelectMany item3 = new BlastResponseSelectMany();
                        item3.BlastScheduleId = message.BlastScheduleId;
                        item3.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).First().Question;
                        item3.Options = new List<BlastResponseSelectManyOption>();
                        var options1 = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == message.AnnouncementMessageId).ToList();
                        foreach (var entry in options1)
                        {
                            BlastResponseSelectManyOption selectOption = new BlastResponseSelectManyOption
                            {
                                Selection = false,
                                Text = entry.Option
                            };
                            item3.Options.Add(selectOption);
                        }
                        return View("ChooseManyOption", item3);
                    default:
                        
                        break;
                }
            }
            return View();
        }

        // POST: BlastResponse/BlastResponseTextOnly
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BlastResponseTextOnly(BlastResponseTextOnly form)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            {
                var entry = db.BlastSchedules.Find(form.BlastScheduleId);
                entry.Responded = true;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BlastIndexForPatient");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BlastResponseTextWithReply(BlastResponseTextWithResponse form)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string UserID = (Session["LoggedUserID"].ToString());
            if (ModelState.IsValid)
            {
                BlastResponse response = new BlastResponse();
                response.BlastScheduleId = form.BlastScheduleId;
                response.ReadByNurse = false;
                response.Response = form.Response;
                response.UserId = UserID;
                response.StudyId = StudyId;
                response.TimeRead= DateTime.Now;
                db.BlastResponses.Add(response);
                var entry = db.BlastSchedules.Find(form.BlastScheduleId);
                entry.Responded = true;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BlastIndexForPatient");
        }
        // POST: BlastResponse/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChooseOneOption(BlastResponseChooseOne form)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string UserID = (Session["LoggedUserID"].ToString());
            if (ModelState.IsValid)
            {
                BlastResponse response = new BlastResponse();
                response.BlastScheduleId = form.BlastScheduleId;
                response.ReadByNurse = false;
                response.Response = form.Response;
                response.UserId = UserID;
                response.StudyId = StudyId;
                response.TimeRead = DateTime.Now;
                db.BlastResponses.Add(response);
                var entry = db.BlastSchedules.Find(form.BlastScheduleId);
                entry.Responded = true;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BlastIndexForPatient");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChooseManyOption(BlastResponseSelectMany form)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string UserID = (Session["LoggedUserID"].ToString());
            if (ModelState.IsValid)
            {
                BlastResponse response = new BlastResponse();
                response.BlastScheduleId = form.BlastScheduleId;
                response.ReadByNurse = false;
                response.UserId = UserID;
                response.StudyId = StudyId;
                response.TimeRead = DateTime.Now;
                string Response = "";
                foreach(var entry1 in form.Options)
                {
                    if (entry1.Selection == true)
                    {
                        Response += " " + entry1.Text;
                    }
                }
                response.Response = Response;
                db.BlastResponses.Add(response);
                var entry = db.BlastSchedules.Find(form.BlastScheduleId);
                entry.Responded = true;
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
            }
            return RedirectToAction("BlastIndexForPatient");
        }
        public ActionResult Create(FormCollection collection)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BlastResponse/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BlastResponse/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BlastResponse/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BlastResponse/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
