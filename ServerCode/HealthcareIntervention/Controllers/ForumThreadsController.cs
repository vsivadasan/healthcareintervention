﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class ForumThreadsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ForumThreads
        public ActionResult Index()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            var Forum = db.Forums.Find(StudyId);
            return View(Forum.Threads.ToList());
        }
        public ActionResult VolunteerIndex()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            var Forum = db.Forums.Find(StudyId);
            return View(Forum.Threads.ToList());
        }

        // GET: ForumThreads/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumThread forumThread = db.ForumThreads.Find(id);
            if (forumThread == null)
            {
                return HttpNotFound();
            }
            return View(forumThread);
        }

        // GET: ForumThreads/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ForumThreads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ForumThreadId,ThreadName")] ForumThread forumThread)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
                 var Forum = db.Forums.Find(StudyId);
                forumThread.Posts = new List<ForumPost>();
                db.ForumThreads.Add(forumThread);
                db.SaveChanges();
                Forum.Threads.Add(forumThread);
                db.Entry(Forum).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("VolunteerIndex");
            }

            return View(forumThread);
        }

        // GET: ForumThreads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumThread forumThread = db.ForumThreads.Find(id);
            if (forumThread == null)
            {
                return HttpNotFound();
            }
            return View(forumThread);
        }

        // POST: ForumThreads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ForumThreadId,ThreadName")] ForumThread forumThread)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(forumThread).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(forumThread);
        }

        // GET: ForumThreads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumThread forumThread = db.ForumThreads.Find(id);
            if (forumThread == null)
            {
                return HttpNotFound();
            }
            return View(forumThread);
        }

        // POST: ForumThreads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            ForumThread forumThread = db.ForumThreads.Find(id);
            db.ForumThreads.Remove(forumThread);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
