﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using HealthcareIntervention.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HealthcareIntervention.Controllers
{
    public class HomeController : Controller
    {
        
        private ApplicationDbContext _context; 
        public HomeController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        public ActionResult Index()
        {
            if(Session["LoggedUserID"]!=null)
            {
                var MyId = Session["LoggedUserID"].ToString();
                //Session["UnreadMessages"] = _context.Messages.Where(x => x.Receiver.Id == MyId).Where(y => y.Read == false).Count();
            }
            else
            {
                return RedirectToAction("SignOff", "Account");
            }
            return View();
        }
        
        public ActionResult Blasts()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        
        public ActionResult Forum()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Messages()
        {
            return View(); 
        }


        [Authorize(Roles = RoleNames.Nurse + "," + RoleNames.Admin)]
        public ActionResult Study()
        {
            return View(); 
        }

        [Authorize(Roles = RoleNames.Admin)]
        public ActionResult UserRoles()
        {

            
           
            var usersInDb = _context.Users.ToList();
            var users = usersInDb.Select(t => new UserRoleViewModel()
            {
                FirstName = t.FirstName, LastName = t.LastName, Email = t.Email, Id = t.Id
            }).ToList();


            return View(users);
        }

        
    }
}