﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HealthcareIntervention.Controllers
{
    public class StudiesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //GET: Add Patients
        public ActionResult AddPatients()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId= Convert.ToInt32(Session["StudyId"].ToString());
            NurseAddPatientsViewModel form = new NurseAddPatientsViewModel();
            form.StudyId = StudyId;
            form.StudyName = db.Studies.Find(StudyId).StudyName;
            form.Nurses = new List<NurseCheckList>();
            form.Patients = new List<PatientCheckList>();
            var NursesInTheStudy = db.NurseStudyMappings.Where(x => x.StudyId == StudyId).Select(x => x.NurseId).ToList();
            foreach (string id in NursesInTheStudy)
            {
                var nurse = db.Users.Find(id);
                NurseCheckList element= new NurseCheckList();
                element.check = true;
                element.FirstName = nurse.FirstName;
                element.LastName = nurse.LastName;
                form.Nurses.Add(element);
            }
            string PatientId = db.Roles.Where(x => x.Name.Contains("Volunteer")).First().Id;//Finding Voluteer ID
            var Volunteers = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(PatientId)).ToList();//Getting list of Patients
            var PatientsCurrentlyInTheStudy = db.PatientStudyMappings.Where(x => x.StudyId == StudyId).ToList();
            var PatientIds = (PatientsCurrentlyInTheStudy.Count > 0)?PatientsCurrentlyInTheStudy.Select(x => x.PatientId).ToList():null;
            foreach (ApplicationUser Volunteer in Volunteers)
            {
                PatientStudyMapping Patient = null;
                if (PatientIds != null)
                {
                    if (PatientIds.Contains(Volunteer.Id)) //If Already part of the study
                    {
                        Patient = PatientsCurrentlyInTheStudy.Where(x => x.PatientId == Volunteer.Id).ToList().First(); //Fetch details
                    }
                } 
                
                DateTime date;
                Boolean Flag;
                if (Patient == null)
                {
                    date = DateTime.Now;
                    Flag = false;
                }
                else
                {
                    date = Patient.StartDate;
                    Flag = Patient.ConfirmedStartDate;
                }
                var entry = new PatientCheckList()
                {
                     check= (Patient!=null)? true : false,
                     StartDate=date,
                     ConfirmedStartDate=Flag,                    
                    FirstName = Volunteer.FirstName,
                    LastName= Volunteer.LastName,
                    Email=Volunteer.Email,
                    Id=Volunteer.Id
                };
                form.Patients.Add(entry);
            }
            return View("AddPatientsToStudy",form);
        }

        //PUT: Add Patients
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPatients(NurseAddPatientsViewModel form1)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            var PatientsInStudy = db.PatientStudyMappings.Where(x => x.StudyId == StudyId).Select(y=>y.PatientId).ToList();
            foreach(PatientCheckList element in form1.Patients)
            {
                if (element.check == true) // If part of study
                {
                    if (PatientsInStudy.Contains(element.Id))//If already present
                    {
                        var patient = db.PatientStudyMappings.Where(x => x.PatientId == element.Id).Where(y => y.StudyId == StudyId).ToList().First();//Getting the record
                        patient.StartDate = element.StartDate;
                        patient.ConfirmedStartDate = element.ConfirmedStartDate;
                        db.Entry(patient).State = EntityState.Modified; //Checking if start date has changed
                        //Code to change Study plan
                        db.SaveChanges();
                        var present = db.BlastSchedules.Where(x => x.StudyId == StudyId).Where(y => y.UserId == element.Id).ToList();
                        if (element.ConfirmedStartDate == true)//If study start date is confirmed
                        {                            
                            if (present.Count==0)// If 1st entry of study start date
                            {
                                MakeBlastPlan(element.Id, StudyId, element.StartDate);
                            }
                        }
                        else
                        {
                            if (present.Count > 0)// Removing entries for the patient
                            {
                                removeBlastPlan(element.Id, StudyId);
                            }
                        }
                    }
                    else //If new in study
                    {
                        var entry = new PatientStudyMapping(); //creating an Entry
                        entry.PatientId = element.Id;
                        entry.StudyId = StudyId;
                        entry.StartDate = element.StartDate;
                        entry.ConfirmedStartDate = element.ConfirmedStartDate;
                        db.PatientStudyMappings.Add(entry); //Adding an entry
                        //Code to make study plan
                        db.SaveChanges();
                        var present = db.BlastSchedules.Where(x => x.StudyId == StudyId).Where(y => y.UserId == element.Id).ToList();
                        if (element.ConfirmedStartDate == true)
                        {
                            
                            if (present.Count==0)
                            {
                                MakeBlastPlan(element.Id, StudyId, element.StartDate);
                            }
                        }
                        else
                        {
                            if (present.Count > 0)// Removing entries for the patient
                            {
                                removeBlastPlan(element.Id, StudyId);
                            }
                        }
                    }
                }
                else   //If not part of study
                {
                    if (PatientsInStudy.Contains(element.Id)) //If Element present
                    {
                        var patient = db.PatientStudyMappings.Where(x => x.PatientId == element.Id).Where(y => y.StudyId == StudyId).ToList().First();//Getting the element
                        //Code to Remove study plan
                        db.PatientStudyMappings.Remove(patient);
                        db.SaveChanges();
                        //Removing from schedule
                        var present = db.BlastSchedules.Where(x => x.StudyId == StudyId).Where(y => y.UserId == element.Id).ToList();
                        if (present.Count > 0)// Removing entries for the patient
                            {
                                removeBlastPlan(element.Id, StudyId);
                            }                        
                    }
                }
            }
            return RedirectToAction("Index", "Home"); 
        }
        //GET: Select Studies
        public ActionResult SelectStudy()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Session["StudyId"] = null;
            Session["Forum"] = null;
            Session["Announcements"] =null;
            Session["Messages"] = null;
            var UserID=Session["LoggedUserID"].ToString();
            var StudiesInvolved = db.NurseStudyMappings.Where(x => x.NurseId == UserID).ToList();
            if (StudiesInvolved.Count() < 1)
            {
                return View("ErrorStudyNotFound");
            }
            else
            {
                List<Study>Studies=new List<Study> ();
                foreach(NurseStudyMapping element in StudiesInvolved)
                {
                   Studies.Add(db.Studies.Find(element.StudyId));
                }
                return View(Studies);
            }                
           
        }
        public ActionResult SelectStudyVolunteer()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Session["StudyId"] = null;
            Session["Forum"] = null;
            Session["Announcements"] = null;
            Session["Messages"] = null;
            Session["UnreadMessages"] = null;
            var UserID = Session["LoggedUserID"].ToString();
            var StudiesInvolved = db.PatientStudyMappings.Where(x => x.PatientId == UserID).ToList();
            if (StudiesInvolved.Count() < 1)
            {
                return View("ErrorStudyNotFound");
            }
            else
            {
                if (StudiesInvolved.Count() == 1) {
                    return RedirectToAction("StudySelected", "Studies", new { id = StudiesInvolved.First().StudyId });
                }
                else
                {
                    List<Study> Studies = new List<Study>();
                    foreach (PatientStudyMapping element in StudiesInvolved)
                    {
                        Studies.Add(db.Studies.Find(element.StudyId));
                    }
                    return View("SelectStudy",Studies);
                }
                
            }

        }
        public ActionResult StudySelected(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Session["StudyId"] = id;
            var study = db.Studies.Find(id);
            Session["Forum"] = study.Forum;
            Session["Announcements"] = study.Announcements;
            Session["Messages"] = study.Messages;
            var MyId = Session["LoggedUserID"].ToString();
            return RedirectToAction("Index", "Home");
        }

        // GET: Studies
        public ActionResult Index()
        {
            var studies = db.Studies.Include(s => s.StudyPlan);
            
            return View(studies.ToList());

        }

        // GET: Studies/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Study study = db.Studies.Find(id);
            if (study == null)
            {
                return HttpNotFound();
            }
            AdminCreateStudyViewModel form = new AdminCreateStudyViewModel();
            form.StudyId = study.StudyId;
            form.Capacity = study.Capacity;
            form.Announcements = study.Announcements;
            form.Forum = study.Forum;
            form.Messages = study.Messages;
            form.StudyName = study.StudyName;
            form.Nurses = new List<NurseCheckList>();
            string nurseroleId = db.Roles.Where(x => x.Name.Contains("Nurse")).First().Id;//Finding Nurse ID
            var nurses = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(nurseroleId)).ToList();//Getting list of nurses
            var NursesInTheStudy = db.NurseStudyMappings.Where(x => x.StudyId == id).Select(x => x.NurseId).ToList();
            foreach (ApplicationUser nurse in nurses)
            {
                var entry = new NurseCheckList()
                {
                    check = NursesInTheStudy.Contains(nurse.Id) ? true : false,
                    FirstName = nurse.FirstName,
                    LastName = nurse.LastName,
                    Id = nurse.Id
                };
                form.Nurses.Add(entry);
            }
            return View(form);
        }

        // GET: Studies/Create
        public ActionResult Create()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            ViewBag.StudyId = new SelectList(db.StudyPlans, "StudyId", "StudyId");
            AdminCreateStudyViewModel form= new AdminCreateStudyViewModel();
            form.Nurses = new List<NurseCheckList>();
            string nurseroleId = db.Roles.Where(x => x.Name.Contains("Nurse")).First().Id;//Finding Nurse ID
            var nurses = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(nurseroleId)).ToList();//Getting list of nurses
            foreach (ApplicationUser nurse in nurses)
            {
                    var entry = new NurseCheckList()
                    {
                        check = false,
                        FirstName = nurse.FirstName,
                        LastName=nurse.LastName,
                        Id=nurse.Id
                    };
                    form.Nurses.Add(entry);
                
            }
            
            return View("create",form);
        }

        // POST: Studies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AdminCreateStudyViewModel form1)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Study study = new Study();
            if (ModelState.IsValid)
            {
                study.Announcements = form1.Announcements;
                study.Capacity = form1.Capacity;
                study.Forum = form1.Forum;
                study.Messages = form1.Messages;
                study.StudyName = form1.StudyName;
                db.Studies.Add(study);
                db.SaveChanges();
                if (form1.Forum == true)
                {
                    Forum MyForum = new Forum();
                    MyForum.Study = study;
                    MyForum.StudyId = study.StudyId;
                    MyForum.Threads = new List<ForumThread>();
                    db.Forums.Add(MyForum);
                    db.SaveChanges();
                }
             
                foreach (NurseCheckList element in form1.Nurses)
                {
                    var Nurses = db.Users.ToList();
                    if (element.check == true)
                    {
                        //element.nurse.StudiesInvolved.Add(study);
                        NurseStudyMapping NewEntry = new NurseStudyMapping();
                        NewEntry.NurseId = element.Id;
                        NewEntry.StudyId = study.StudyId;
                        db.NurseStudyMappings.Add(NewEntry);
                        db.SaveChanges();
                    }
                }
               
                return RedirectToAction("Index");
            }

            ViewBag.StudyId = new SelectList(db.StudyPlans, "StudyId", "StudyId", study.StudyId);
            return View(study);
        }

        // GET: Studies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Study study = db.Studies.Find(id);
            if (study == null)
            {
                return HttpNotFound();
            }
            AdminCreateStudyViewModel form = new AdminCreateStudyViewModel();
            form.StudyId = study.StudyId;
            form.Capacity = study.Capacity;
            form.Announcements = study.Announcements;
            form.Forum = study.Forum;
            form.Messages = study.Messages;
            form.StudyName = study.StudyName;
            form.Nurses = new List<NurseCheckList>();
            string nurseroleId = db.Roles.Where(x => x.Name.Contains("Nurse")).First().Id;//Finding Nurse ID
            var nurses = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(nurseroleId)).ToList();//Getting list of nurses
            var NursesInTheStudy = db.NurseStudyMappings.Where(x => x.StudyId==id).Select(x=>x.NurseId).ToList();
            foreach (ApplicationUser nurse in nurses)
            {
                var entry = new NurseCheckList()
                {
                    check = NursesInTheStudy.Contains(nurse.Id) ? true : false,
                    FirstName = nurse.FirstName,
                    LastName = nurse.LastName,
                    Id=nurse.Id
                };
                form.Nurses.Add(entry);
            }
            ViewBag.StudyId = new SelectList(db.StudyPlans, "StudyId", "StudyId", study.StudyId);
            return View(form);
        }

        // POST: Studies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AdminCreateStudyViewModel form1)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Study study = db.Studies.Find(form1.StudyId);
            study.Announcements = form1.Announcements;
            study.Capacity = form1.Capacity;
            study.Forum = form1.Forum;
            study.Messages = form1.Messages;
            study.StudyName = form1.StudyName;
            var NursesMappings= db.NurseStudyMappings.Where(x => x.StudyId == form1.StudyId).Select(x => x.NurseId).ToList();
            if (ModelState.IsValid)
            {
                db.Entry(study).State = EntityState.Modified;
                db.SaveChanges();
                foreach(NurseCheckList element in form1.Nurses)
                {
                    if (element.check)
                    {
                        if (!NursesMappings.Contains(element.Id)){
                            NurseStudyMapping NewEntry = new NurseStudyMapping();
                            NewEntry.NurseId = element.Id;
                            NewEntry.StudyId = study.StudyId;
                            db.NurseStudyMappings.Add(NewEntry);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        if (NursesMappings.Contains(element.Id))
                        {
                            NurseStudyMapping Entry = db.NurseStudyMappings.Where(x=>x.NurseId==element.Id).Where(x=>x.StudyId==study.StudyId).First();
                            db.NurseStudyMappings.Remove(Entry);
                            db.SaveChanges();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            ViewBag.StudyId = new SelectList(db.StudyPlans, "StudyId", "StudyId", study.StudyId);
            return View(study);
        }

        // GET: Studies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Study study = db.Studies.Find(id);
            if (study == null)
            {
                return HttpNotFound();
            }

            return View(study);
        }

        // POST: Studies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var StudyPlan = db.StudyPlans.Find(id);
            var NurseMapEntries = db.NurseStudyMappings.Where(x => x.StudyId == id).ToList();
            var PatientMapping = db.PatientStudyMappings.Where(y => y.StudyId == id).ToList();

            Study study = db.Studies.Find(id);
            db.Studies.Remove(study);
            db.SaveChanges();
            var NursesMappings = db.NurseStudyMappings.Where(x => x.StudyId == id).ToList();
            foreach(NurseStudyMapping element in NursesMappings)
            {
                db.NurseStudyMappings.Remove(element);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
        public void MakeBlastPlan(string PatientId,int studyId,DateTime StartDate)
        {
            if (Session["LoggedUserID"] == null)
            {
                 RedirectToAction("SignOff", "Account");
            }
            var User = db.Users.Find(PatientId);
            var studyPlan = db.StudyPlans.Find(studyId);
            foreach (var Day in studyPlan.AnnouncementDays)
            {
                foreach(var Blast in Day.AnnouncementMessages)
                {                   
                    BlastSchedule entry = new BlastSchedule();
                    entry.AnnouncementMessageId = Blast.AnnouncementMessageId;
                    entry.BlastType = Blast.BlastType;
                    entry.Responded = false;
                    entry.Visible = false;
                    entry.UserId = PatientId;
                    entry.StudyId = studyId;
                    DateTime temp= StartDate.AddDays((Day.DayNo) - 1); 
                    entry.SendTime = new DateTime(temp.Year,temp.Month,temp.Day,Blast.Time.Hour,Blast.Time.Minute,Blast.Time.Second);
                    db.BlastSchedules.Add(entry);
                    db.SaveChanges();                    
                }
            }

        }
        public void removeBlastPlan(string patientId, int studyId)
        {
            if (Session["LoggedUserID"] == null)
            {
                RedirectToAction("SignOff", "Account");
            }
            var User = db.Users.Find(patientId);
            var studyPlan = db.StudyPlans.Find(studyId);
            List<BlastSchedule> entries = db.BlastSchedules.Where(x => x.StudyId == studyId).Where(y => y.UserId == patientId).ToList();
            var Responses = db.BlastResponses.Where(x => x.UserId == patientId).Where(y => y.StudyId == studyId).ToList();
            db.BlastResponses.RemoveRange(Responses);
            db.BlastSchedules.RemoveRange(entries);
            db.SaveChanges();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
