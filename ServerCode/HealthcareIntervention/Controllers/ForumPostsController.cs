﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class ForumPostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ForumPosts
        public ActionResult Index(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int ThreadID = (int)id;
            var Posts = db.ForumThreads.Find(id).Posts.ToList();
            ViewBag.ThreadID = ThreadID;
            return View(Posts);
        }
        public ActionResult VolunteerIndex(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int ThreadID = (int)id;
            var Posts = db.ForumThreads.Find(id).Posts.ToList();
            ViewBag.ThreadID = ThreadID;
            return View(Posts);
        }

        // GET: ForumPosts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumPost forumPost = db.ForumPosts.Find(id);
            if (forumPost == null)
            {
                return HttpNotFound();
            }
            return View(forumPost);
        }

        // GET: ForumPosts/Create
       

        // POST: ForumPosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ForumPostViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                if (Session["LoggedUserID"] == null)
                {
                    return RedirectToAction("SignOff", "Account");
                }
                string UserID= (Session["LoggedUserID"].ToString());
                var User = db.Users.Find(UserID);
                ForumPost Post = new ForumPost();
                ForumThread CurrentThread = db.ForumThreads.Find(ViewModel.ThreadID);
                Post.Author = User.FirstName.ToString()+" "+User.LastName.ToString();
                Post.Content = ViewModel.Content;
                Post.PostingTime= DateTime.Now;
                db.ForumPosts.Add(Post);
                db.SaveChanges();
                CurrentThread.Posts.Add(Post);
                db.Entry(CurrentThread).State = EntityState.Modified;
                db.SaveChanges();
                int id = ViewModel.ThreadID;
                var Role = (Session["loggedUserType"].ToString());
                if (Role == "Nurse")
                {
                    return RedirectToAction("Index", "ForumPosts", new { id = ViewModel.ThreadID });
                }
                else
                {
                    return RedirectToAction("VolunteerIndex", "ForumPosts", new { id = ViewModel.ThreadID });
                }
                
            }

            return View();
        }

        // GET: ForumPosts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumPost forumPost = db.ForumPosts.Find(id);
            if (forumPost == null)
            {
                return HttpNotFound();
            }
            return View(forumPost);
        }

        // POST: ForumPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ForumPostId,PostingTime,Content")] ForumPost forumPost)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(forumPost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(forumPost);
        }

        // GET: ForumPosts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ForumPost forumPost = db.ForumPosts.Find(id);
            if (forumPost == null)
            {
                return HttpNotFound();
            }
            return View(forumPost);
        }

        // POST: ForumPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            ForumPost forumPost = db.ForumPosts.Find(id);
            db.ForumPosts.Remove(forumPost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
