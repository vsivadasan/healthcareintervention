﻿using HealthcareIntervention.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Controllers
{
    public class BlastTemplateOptionsQuestionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: BlastTemplateOptionsQuestion
        public ActionResult Index()
        {
            return View();
        }

        // GET: BlastTemplateOptionsQuestion/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult Create(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var BlastId = (int)id;
            BlastTemplateOptionsQuestion model = new BlastTemplateOptionsQuestion();
            model.AnnouncementMessageId = BlastId;
            return View("CreateBlastWithOptionsQuestion", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BlastTemplateOptionsQuestion model)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.BlastTemplateOptionsQuestions.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Create","BlastTemplateOptions", new { id = model.AnnouncementMessageId});
        }

       
    }
}
