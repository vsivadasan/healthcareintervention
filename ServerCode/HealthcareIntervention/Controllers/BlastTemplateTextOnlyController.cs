﻿using HealthcareIntervention.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Controllers
{
    public class BlastTemplateTextOnlyController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: BlastTemplateTextOnly
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var BlastId = (int)id;
            BlastTemplateTextOnly template = new BlastTemplateTextOnly();
            template.AnnouncementMessageId = BlastId;
            return View("CreateBlastTextOnly", template);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BlastTemplateTextOnly model)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.BlastTemplateTextOnlies.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("GetStudyPlan", "StudyPlans");
        }
    }
}