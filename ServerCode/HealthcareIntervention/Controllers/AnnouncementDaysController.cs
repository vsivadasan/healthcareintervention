﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class AnnouncementDaysController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //GET Add Days
        public ActionResult AddDay(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyPlanId = Convert.ToInt32(Session["StudyId"].ToString());
            StudyPlan plan = db.StudyPlans.Find(StudyPlanId);
            int lastDay = 0;
            if (id != null)
            {
                lastDay = (int)id;
                var DaysToMove1Step = db.AnnouncementDays.Where(x=>x.StudyPlan.StudyId==StudyPlanId).Where(x => x.DayNo > lastDay).ToList();
                if (DaysToMove1Step != null)
                {
                    foreach(AnnouncementDay day in DaysToMove1Step)
                    {
                        day.DayNo = day.DayNo + 1;
                        db.Entry(day).State= EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            else if (plan.AnnouncementDays.Count() > 0)
            {
                lastDay = plan.AnnouncementDays.Count();
            }
            AnnouncementDay Newday = new AnnouncementDay();
            Newday.AnnouncementMessages = new List<AnnouncementMessage>();
            Newday.DayNo = lastDay + 1;
            Newday.StudyPlan = plan;
            db.AnnouncementDays.Add(Newday);
            db.SaveChanges();
            plan.AnnouncementDays.Add(Newday);
            db.Entry(plan).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("GetStudyPlan", "StudyPlans");
        }
        public ActionResult RemoveDay(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            int StudyPlanId = Convert.ToInt32(Session["StudyId"].ToString());
            int lastDay = (int)id;
            AnnouncementDay Day = db.AnnouncementDays.Where(x => x.DayNo == lastDay).ToList().First();
            //Remove Messages
            List<AnnouncementMessage> MessagesDelete = db.AnnouncementMessages.Where(x => x.AnnouncementDay.AnnouncementDayId == Day.AnnouncementDayId).ToList();
            foreach (AnnouncementMessage Message in MessagesDelete)
            {
                switch (Message.BlastType)
                {
                    case "TextOnly":
                        var Blasts = db.BlastTemplateTextOnlies.Where(x => x.AnnouncementMessageId == Message.AnnouncementMessageId).ToList();
                        if (Blasts.Count > 0)
                        {
                            var Blast =Blasts.First();
                            db.BlastTemplateTextOnlies.Remove(Blast);
                            db.SaveChanges();
                        }
                        
                        break;
                    case "TextWithResponse":
                        var Blasts1 = db.BlastTemplateTextWithResponses.Where(x => x.AnnouncementMessageId == Message.AnnouncementMessageId).ToList();
                        if (Blasts1.Count > 0)
                        {
                            var Blast1 = Blasts1.First();
                            db.BlastTemplateTextWithResponses.Remove(Blast1);
                            db.SaveChanges();
                        }
                        
                        break;
                    case "ChooseOneFromList":
                        var Blast2 = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == Message.AnnouncementMessageId).ToList();
                        foreach (var item in Blast2)
                        {
                            db.BlastTemplateOptionses.Remove(item);
                            db.SaveChanges();
                        }
                        break;
                    case "ChooseSeveralFromList":
                        var Blast3 = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == Message.AnnouncementMessageId).ToList();
                        foreach (var item in Blast3)
                        {
                            db.BlastTemplateOptionses.Remove(item);
                            db.SaveChanges();
                        }
                        break;
                    default:
                        break;
                }
                db.AnnouncementMessages.Remove(Message);
                db.SaveChanges();
            }
            var DaysToMove1Step = db.AnnouncementDays.Where(x => x.StudyPlan.StudyId == StudyPlanId).Where(x => x.DayNo > lastDay).ToList();
            if (DaysToMove1Step != null)
            {
                foreach (AnnouncementDay day in DaysToMove1Step)
                {
                    day.DayNo = day.DayNo - 1;
                    db.Entry(day).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            
            db.AnnouncementDays.Remove(Day);
            db.SaveChanges();
            return RedirectToAction("GetStudyPlan", "StudyPlans");
        }
        // GET: AnnouncementDays
        public ActionResult Index()
        {
            return View(db.AnnouncementDays.ToList());
        }

        // GET: AnnouncementDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementDay announcementDay = db.AnnouncementDays.Find(id);
            if (announcementDay == null)
            {
                return HttpNotFound();
            }
            return View(announcementDay);
        }

        // GET: AnnouncementDays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AnnouncementDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnnouncementDayId,DayNo")] AnnouncementDay announcementDay)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.AnnouncementDays.Add(announcementDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(announcementDay);
        }

        // GET: AnnouncementDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementDay announcementDay = db.AnnouncementDays.Find(id);
            if (announcementDay == null)
            {
                return HttpNotFound();
            }
            return View(announcementDay);
        }

        // POST: AnnouncementDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnnouncementDayId,DayNo")] AnnouncementDay announcementDay)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(announcementDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(announcementDay);
        }

        // GET: AnnouncementDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementDay announcementDay = db.AnnouncementDays.Find(id);
            if (announcementDay == null)
            {
                return HttpNotFound();
            }
            return View(announcementDay);
        }

        // POST: AnnouncementDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AnnouncementDay announcementDay = db.AnnouncementDays.Find(id);
            db.AnnouncementDays.Remove(announcementDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
