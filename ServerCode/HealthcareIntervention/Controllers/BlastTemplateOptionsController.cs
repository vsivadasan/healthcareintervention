﻿using HealthcareIntervention.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Controllers
{
    public class BlastTemplateOptionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: BlastTemplateOptions
        public ActionResult Index()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            return View();
        }
        public ActionResult Create(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var BlastId = (int)id;
            ViewBag.Question = db.BlastTemplateOptionsQuestions.Where(x => x.AnnouncementMessageId == BlastId).First().Question;
            BlastTemplateOptionsCreate model = new BlastTemplateOptionsCreate();
            model.ExistingOptions = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == BlastId).ToList();
            model.NewOptions = new BlastTemplateOptions();
            model.NewOptions.AnnouncementMessageId = BlastId;
            return View("CreateBlastWithOptions", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BlastTemplateOptionsCreate model)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.BlastTemplateOptionses.Add(model.NewOptions);
                db.SaveChanges();
            }
            return RedirectToAction("Create", new { id = model.NewOptions.AnnouncementMessageId });
        }
    }
}