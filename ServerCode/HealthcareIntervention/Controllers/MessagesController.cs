﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class MessagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //GET: Patients

            public ActionResult SelectPatients()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var MyId = (Session["LoggedUserID"].ToString());
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string PatientId = db.Roles.Where(x => x.Name.Contains("Volunteer")).First().Id;//Finding Voluteer ID
            var Volunteers = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(PatientId)).ToList();//Getting list of Patients
            var PatientsCurrentlyInTheStudy = db.PatientStudyMappings.Where(x => x.StudyId == StudyId).ToList();
            var PatientIds = (PatientsCurrentlyInTheStudy.Count > 0) ? PatientsCurrentlyInTheStudy.Select(x => x.PatientId).ToList() : null;
            List<SelectPatientsViewModel> Patients = new List<SelectPatientsViewModel>();
            if (PatientIds != null)
            {
                foreach (ApplicationUser Volunteer in Volunteers)
                {
                    if (PatientIds.Contains(Volunteer.Id))
                    {
                        SelectPatientsViewModel person = new SelectPatientsViewModel();
                        person.id = Volunteer.Id;
                        person.Name = Volunteer.FirstName + " " + Volunteer.LastName;
                        person.count = db.Messages.Where(x => x.Receiver.Id == MyId).Where(y => y.Sender.Id == Volunteer.Id).Where(x => x.Read == false).Count();
                        Patients.Add(person);
                    }
                }
            }
            Patients = Patients.OrderByDescending(x => x.count).ToList();
            return View("SelectPatients",Patients);
        }

        //Get SelectNurses
        public ActionResult SelectNurses()
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var MyId = (Session["LoggedUserID"].ToString());
            int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
            string NurseId = db.Roles.Where(x => x.Name.Contains("Nurse")).First().Id;//Finding Nurse ID
            var Nurses = db.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(NurseId)).ToList();//Getting list of Nurses
            var NursesCurrentlyInTheStudy = db.NurseStudyMappings.Where(x => x.StudyId == StudyId).ToList();
            var NurseIds = (NursesCurrentlyInTheStudy.Count > 0) ? NursesCurrentlyInTheStudy.Select(x => x.NurseId).ToList() : null;
            List<SelectPatientsViewModel> Patients = new List<SelectPatientsViewModel>();
            if (NurseIds != null)
            {
                foreach (ApplicationUser Nurse in Nurses)
                {
                    if (NurseIds.Contains(Nurse.Id))
                    {
                        SelectPatientsViewModel person = new SelectPatientsViewModel();
                        person.id = Nurse.Id;
                        person.Name ="Nurse "+Nurse.FirstName + " " + Nurse.LastName;
                        person.count = db.Messages.Where(x => x.Receiver.Id == MyId).Where(y => y.Sender.Id == Nurse.Id).Where(x=>x.Read==false).Count();
                        Patients.Add(person);
                    }
                }
            }
            Patients = Patients.OrderByDescending(x => x.count).ToList();
            return View("SelectPatients",Patients);
        }
        // GET: Messages
        public ActionResult Index(string id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            var MyId = Session["LoggedUserID"].ToString();
            var OtherEnd = db.Users.Find(id);
            ViewBag.OtherEnd = OtherEnd.FirstName + " " + OtherEnd.LastName;
            ViewBag.ReceiverId = id;
            List<Message> Messages = new List<Message>();
            Messages = db.Messages.Where(x => x.Sender.Id == MyId).Where(y=>y.Receiver.Id==id).ToList();
            var MessagesToMe = db.Messages.Where(x => x.Sender.Id == id).Where(y => y.Receiver.Id == MyId).ToList();
            int Menucount = Convert.ToInt32(Session["UnreadMessages"].ToString());
            foreach (var item in MessagesToMe.Where(x=>x.Read!=true))
            {
                item.Read = true;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                --Menucount;
            }
            Session["UnreadMessages"] = Menucount;
            Messages.AddRange(MessagesToMe);
            Messages=Messages.OrderBy(z=>z.SendTime).ToList();
            return View(Messages);
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SendMessageViewModel model)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                var SenderId = Session["LoggedUserID"].ToString();
                var ReceiverId = model.ReceiverId;
                var Sender = db.Users.Find(SenderId);
                var Receiver = db.Users.Find(ReceiverId);
                Message MyMessage = new Message();
                MyMessage.Read = false;
                MyMessage.Receiver = Receiver;
                MyMessage.Sender = Sender;
                MyMessage.Content = model.Content;
                MyMessage.SendTime = DateTime.Now;
                db.Messages.Add(MyMessage);
                db.SaveChanges();
            
            }

            return RedirectToAction("Index", "Messages", new { id = model.ReceiverId});
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MessageID,SendTime,Read,Content")] Message message)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(message);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
