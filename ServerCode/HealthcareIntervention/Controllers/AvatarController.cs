﻿using HealthcareIntervention.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Controllers
{
    public class AvatarController : Controller
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        // GET: Avatar
        public ActionResult Index()
        {
            return View();
        }

        // GET: Avatar/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Avatar/Create
        public void Create(string ID)
        {
            //var absolutePath = Server.MapPath("~/App_Data/Images/avatar.jpg");
            //var File = System.IO.File.ReadAllBytes(absolutePath);
            //var avatar = new Avatar
            //{
            //    Image = File,
            //    userId = ID
            //};
            //context.Avatars.Add(avatar);
            //context.SaveChanges();
            return;
        }

   
        // GET: Avatar/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Avatar/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Avatar/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Avatar/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
