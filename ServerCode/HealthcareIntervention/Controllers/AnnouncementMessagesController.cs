﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HealthcareIntervention.Models;

namespace HealthcareIntervention.Controllers
{
    public class AnnouncementMessagesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //GET: AddAnnouncementMessages
        
        // GET: AnnouncementMessages
        public ActionResult Index()
        {
            return View(db.AnnouncementMessages.ToList());
        }

        // GET: AnnouncementMessages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMessage announcementMessage = db.AnnouncementMessages.Find(id);
            if (announcementMessage == null)
            {
                return HttpNotFound();
            }
            return View(announcementMessage);
        }

        // GET: AnnouncementMessages/Create
        public ActionResult Create(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            AnnouncementMessageViewModel NewMessage = new AnnouncementMessageViewModel();
            NewMessage.AnnouncementDay = (int)id;
            List<SelectListItem> Hours=new List<SelectListItem>();
            List<SelectListItem> Minutes= new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                Hours.Add(new SelectListItem
                {
                    Value=i.ToString(),
                    Text=i.ToString()
                });
            }
            for(int i=0;i<60; i+= 5)
            {
                Minutes.Add(new SelectListItem
                {
                    Value = i.ToString(),
                    Text = i.ToString()
                });
            }
            NewMessage.Hours = Hours;
            NewMessage.Minutes = Minutes;
            return View(NewMessage);
        }

        // POST: AnnouncementMessages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( AnnouncementMessageViewModel model)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                int StudyId = Convert.ToInt32(Session["StudyId"].ToString());
                AnnouncementMessage announcementMessage = new AnnouncementMessage();
                announcementMessage.AnnouncementName = model.AnnouncementName;
                var Day = db.AnnouncementDays.Where(x => x.StudyPlan.StudyId == StudyId).Where(x => x.DayNo == model.AnnouncementDay).ToList().First();
                announcementMessage.AnnouncementDay = Day;
                announcementMessage.BlastType = model.BlastType;
                var TimeNow = DateTime.Now;
                var hour = (model.AMorPM == "PM") ? 12 + model.Hour : model.Hour;
                hour = (hour > 23) ? 0 : hour;
                var Time = new DateTime(2000,01 ,01,hour, model.Minute, 0);
                announcementMessage.Time = Time;
                db.AnnouncementMessages.Add(announcementMessage);
                db.SaveChanges();
                Day.AnnouncementMessages.Add(announcementMessage);
                db.Entry(Day).State = EntityState.Modified;
                db.SaveChanges();
                switch (model.BlastType)
                {
                    case "TextOnly":
                        return RedirectToAction("Create", "BlastTemplateTextOnly", new { id = announcementMessage.AnnouncementMessageId });
                    case "TextWithResponse":
                        return RedirectToAction("Create", "BlastTemplateTextWithResponse", new { id = announcementMessage.AnnouncementMessageId });                        
                    case "ChooseOneFromList":
                        return RedirectToAction("Create", "BlastTemplateOptionsQuestion", new { id = announcementMessage.AnnouncementMessageId });
                    case "ChooseSeveralFromList":
                        return RedirectToAction("Create", "BlastTemplateOptionsQuestion", new { id = announcementMessage.AnnouncementMessageId });
                    default:
                        break;
                }
            }
            

            return RedirectToAction("GetStudyPlan", "StudyPlans");
        }

        // GET: AnnouncementMessages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMessage announcementMessage = db.AnnouncementMessages.Find(id);
            if (announcementMessage == null)
            {
                return HttpNotFound();
            }
            return View(announcementMessage);
        }

        // POST: AnnouncementMessages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AnnouncementMessageId,Time,BlastMessage")] AnnouncementMessage announcementMessage)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (ModelState.IsValid)
            {
                db.Entry(announcementMessage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(announcementMessage);
        }

        // GET: AnnouncementMessages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AnnouncementMessage announcementMessage = db.AnnouncementMessages.Find(id);
            if (announcementMessage == null)
            {
                return HttpNotFound();
            }
            switch (announcementMessage.BlastType)
            {
                case "TextOnly":
                    var Blast = db.BlastTemplateTextOnlies.Where(x => x.AnnouncementMessageId == announcementMessage.AnnouncementMessageId).ToList().First();
                    db.BlastTemplateTextOnlies.Remove(Blast);
                    db.SaveChanges();
                    break;
                case "TextWithResponse":
                    var Blasts1 = db.BlastTemplateTextWithResponses.Where(x => x.AnnouncementMessageId == announcementMessage.AnnouncementMessageId).ToList();
                    if (Blasts1.Count > 0)
                    {
                        var Blast1 = Blasts1.First();
                        db.BlastTemplateTextWithResponses.Remove(Blast1);
                        db.SaveChanges();
                    }
                    
                    break;
                case "ChooseOneFromList":
                    var Blast2 = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == announcementMessage.AnnouncementMessageId).ToList();
                    foreach(var item in Blast2)
                    {
                        db.BlastTemplateOptionses.Remove(item);
                        db.SaveChanges();
                    }                    
                    break;
                case "ChooseSeveralFromList":
                    var Blast3 = db.BlastTemplateOptionses.Where(x => x.AnnouncementMessageId == announcementMessage.AnnouncementMessageId).ToList();
                    foreach (var item in Blast3)
                    {
                        db.BlastTemplateOptionses.Remove(item);
                        db.SaveChanges();
                    }
                    break;
                default:
                    break;
            }
            
            db.AnnouncementMessages.Remove(announcementMessage);
            db.SaveChanges();
            return RedirectToAction("GetStudyPlan", "StudyPlans");
        }

        // POST: AnnouncementMessages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Session["LoggedUserID"] == null)
            {
                return RedirectToAction("SignOff", "Account");
            }
            AnnouncementMessage announcementMessage = db.AnnouncementMessages.Find(id);
            db.AnnouncementMessages.Remove(announcementMessage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
