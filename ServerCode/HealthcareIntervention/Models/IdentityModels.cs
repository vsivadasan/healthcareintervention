﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections;
using System.Collections.Generic;

namespace HealthcareIntervention.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public virtual Avatar Avatar { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<Avatar> Avatars { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.Study> Studies { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.StudyPlan> StudyPlans { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.AnnouncementDay> AnnouncementDays { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.AnnouncementMessage> AnnouncementMessages { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.NurseStudyMapping> NurseStudyMappings { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.PatientStudyMapping> PatientStudyMappings { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.Message> Messages { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.ForumThread> ForumThreads { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.ForumPost> ForumPosts { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.Forum> Forums { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastTemplateTextOnly> BlastTemplateTextOnlies { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastTemplateTextWithResponse> BlastTemplateTextWithResponses { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastSchedule> BlastSchedules { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastTemplateOptions> BlastTemplateOptionses { get; set; }
        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastResponse> BlastResponses { get; set; }

        public System.Data.Entity.DbSet<HealthcareIntervention.Models.BlastTemplateOptionsQuestion> BlastTemplateOptionsQuestions { get; set; }
    }
}