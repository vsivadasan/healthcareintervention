﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class ForumPost
    {
        public int ForumPostId { get; set; }
        public string Author { get; set; }
        public DateTime PostingTime { get; set; }
        public string Content { get; set; }

    }
}