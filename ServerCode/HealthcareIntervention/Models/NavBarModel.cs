﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class NavBarModel
    {
       public string VolunteerNavBar = "_VolunteerNavBar";
       public string NurseNavBar = "_NurseNavBar";
       public string AdminNavBar = "_AdminNavBar";
       public string LoggedoffBar = "_Loggedoff"; 
    }
}