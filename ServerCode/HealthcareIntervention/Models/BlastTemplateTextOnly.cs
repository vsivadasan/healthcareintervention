﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastTemplateTextOnly
    {
        public int BlastTemplateTextOnlyId {get;set;}
        public string BlastMessage { get; set; }
        public int AnnouncementMessageId{ get; set; }

    }
}