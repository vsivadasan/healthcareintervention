﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class AnnouncementDay
    {
        public int AnnouncementDayId { get; set; }
        public int DayNo { get; set; }
        public virtual ICollection<AnnouncementMessage> AnnouncementMessages { get; set; }
        public virtual StudyPlan StudyPlan { get; set; }
    }
}