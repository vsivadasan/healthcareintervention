﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HealthcareIntervention.Models
{
    public class NurseCheckList
    {
        public Boolean check { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Id { get; set; }
    }
    public class PatientCheckList
    {
        public Boolean check { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public Boolean ConfirmedStartDate { get; set; }
        public DateTime StartDate { get; set; }
    }
    public class AdminCreateStudyViewModel
    {
        [Required]
        public int StudyId { get; set; }
        public string StudyName { get; set; }
        public Boolean Forum { get; set; }
        public Boolean Messages { get; set; }
        public Boolean Announcements { get; set; }
        [Required]
        public int Capacity { get; set; }

        public virtual List<NurseCheckList> Nurses { get; set; }
    }
    public class NurseAddPatientsViewModel
    {
        [Required]
        public int StudyId { get; set; }
        public string StudyName { get; set; }
        public virtual List<PatientCheckList>Patients { get; set; }
        public virtual List<NurseCheckList>Nurses { get; set; }
    }
    public class DayAndEvent
    {
        public int Day { get; set; }
        public List<Event>Events{get;set;}

    }
    public class Event
    {
        public string EventName { get; set; }
        public string EventType { get; set; }
        public int AnnouncementId { get; set; }
        public DateTime Time { get; set; }
    }
    public class StudyPlanViewModel
    {
        public int StudyId { get; set; }
        public virtual Study Study { get; set; }
        public List<DayAndEvent> Days { get; set; }
    }
    public class AnnouncementMessageViewModel
    {
        public IEnumerable<SelectListItem> Hours { get; set; }
        public IEnumerable<SelectListItem> Minutes { get; set; }
        public string AMorPM {get;set;}
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int AnnouncementDay { get; set; }
        [Display(Name = "Blast Type")]
        public string BlastType { get; set; }
        [Display(Name = "Blast Name")]
        public string AnnouncementName { get; set; }
        
    }
    public class SelectPatientsViewModel
    {
        [Key]
        public string id;
        public string Name;
        public int count;
        
    }
    public class SendMessageViewModel
    {
        public string ReceiverId { get; set; }
        public string Content { get; set; }
    }
    public class ForumPostViewModel
    {
        public int ThreadID { get; set; }
        public string Content { get; set; }

    }
    public class BlastTemplateOptionsCreate
    {
        public IEnumerable<BlastTemplateOptions> ExistingOptions { get; set; }
        public BlastTemplateOptions NewOptions { get; set; }

    }
  
    public class BlastListing
    {
        public string BlastName { set; get; }
        public int BlastScheduleId { get; set; }
    }
    public class BlastResponseTextOnly
    {
        public int BlastScheduleId { get; set; }
        public string Text { get; set; }
    }
    public class BlastResponseTextWithResponse
    {
        public int BlastScheduleId { get; set; }
        public string Text { get; set; }
        [Required]
        public string Response { get; set; }
    }

    public class BlastResponseChooseOne
    {
        public int BlastScheduleId { get; set; }
        public string Question { get; set; }
        public List<string> Responses { get; set; }
        public string Response { get; set; }
    }
    public class BlastResponseSelectManyOption
    {
        public Boolean Selection{get;set;}
        public string Text { get; set; }
    }
    public class BlastResponseSelectMany
    {
        public int BlastScheduleId { get; set; }
        public string Question { get; set; }
        public List<BlastResponseSelectManyOption> Options { get; set; } 

    }
    public class BlastListingNurseEntries 
    {
        public string UserName { get; set; } 
        public string Name { get; set; }
        public int unread { get; set; }
    }

    public class BlastResponseEntry
    {
        public Boolean New { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime ReadTime { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
    public class BlastResponses
    {
        public string UserId { get; set; }
        public List<BlastResponseEntry> Answered { get; set; }
        public List<BlastResponseEntry> UnAnswered { get; set; }
    }
   
}