﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class Message
    {
        public int MessageID { get; set; }
        public DateTime SendTime { get; set; }
        public virtual ApplicationUser Sender { get; set; }
        public virtual ApplicationUser Receiver { get; set; }
        public Boolean Read { get; set; }
        public string Content { get; set; }
    }
}