﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class Avatar
    {
        public int AvatarID { get; set; }
        [Column(TypeName = "image")]
        public byte[] Image { get; set; }
        public string userId { get; set; }
    }
}