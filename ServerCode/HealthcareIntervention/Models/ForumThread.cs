﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class ForumThread
    {
        public int ForumThreadId { get; set; }
        [Required]
        public string ThreadName { get; set; }
        public virtual ICollection<ForumPost> Posts { get; set; }
    }
}