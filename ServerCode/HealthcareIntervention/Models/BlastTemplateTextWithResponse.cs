﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastTemplateTextWithResponse
    {
        public int BlastTemplateTextWithResponseId { get; set; }
        public string BlastMessage { get; set; }
        public int AnnouncementMessageId { get; set; }
    }
}