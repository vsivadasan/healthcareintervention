﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class RoleManagementViewModels
    {
        public String FirstName{ get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String UserId { get; set; }
        public List<String> Roles { get; set; }
}
}