﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class RoleNames
    {
        public const string Admin = "Admin";
        public const string Nurse = "Nurse";
        public const string Volunteer = "Volunteer"; 
    }
}