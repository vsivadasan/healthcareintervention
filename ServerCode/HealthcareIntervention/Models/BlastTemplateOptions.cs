﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastTemplateOptions
    {
        public int BlastTemplateOptionsId { get; set; }
        [Required]
        public string Option { get; set; }
        public int AnnouncementMessageId { get; set; }
    }
}