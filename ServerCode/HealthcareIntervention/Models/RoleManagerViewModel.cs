﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class RoleManagerViewModel
    {
        public IList<string> RoleNames { get; set; }
        public string Id { get; set; }
    }
}