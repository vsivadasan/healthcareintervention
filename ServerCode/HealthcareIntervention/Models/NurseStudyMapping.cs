﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class NurseStudyMapping
    {   [Key]
        public int NurseStudyId { get; set; }
        public string NurseId { get; set; }
        public int StudyId { get; set; }
    }
}