﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastSchedule
    {
        public int BlastScheduleId { get; set; }
        public string UserId { get; set; }
        public int AnnouncementMessageId { get; set; }
        public string BlastType { get; set; }
        public DateTime SendTime { get; set; }
        public int StudyId { get; set; }
        public Boolean Visible { get; set; }
        public Boolean Responded { get; set; }
    }
}