﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastTemplateOptionsQuestion
    {
        public int BlastTemplateOptionsQuestionId { get; set; }
        [Required]
        public string Question { get; set; }
        public int AnnouncementMessageId { get; set; }
    }
}