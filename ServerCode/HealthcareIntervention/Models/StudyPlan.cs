﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class StudyPlan
    {
        
        [Key,ForeignKey("Study")]
        public int StudyId { get; set; }
        public virtual Study Study { get; set; }
        public virtual ICollection<AnnouncementDay> AnnouncementDays { get; set; }
    }
}