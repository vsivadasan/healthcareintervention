﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class PatientStudyMapping
    {
        [Key]
        public int PatientStudyId { get; set; }
        public string PatientId { get; set; }
        public int StudyId { get; set; }
        public DateTime StartDate { get; set; }
        public Boolean ConfirmedStartDate { get; set; }
    }
}