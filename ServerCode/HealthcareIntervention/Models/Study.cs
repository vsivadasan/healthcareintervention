﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class Study
    {
        public int StudyId { get; set; }
        public string StudyName { get; set; }
        public Boolean Forum { get; set; }
        public Boolean Messages { get; set; }
        public Boolean Announcements { get; set; }
        public int Capacity { get; set; }


        
        public virtual StudyPlan StudyPlan { get; set; }

    }
}