﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class AnnouncementMessage
    {
        
        public int AnnouncementMessageId { get; set; }
        public DateTime Time { get; set; }
        [Display(Name = "Message")]
        public string BlastType { get; set; }
        [Display(Name = "Announcement Name")]
        public string AnnouncementName { get; set; }
        public virtual AnnouncementDay AnnouncementDay { get; set; }

    }
}