﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HealthcareIntervention.Models
{
    public class BlastResponse
    {
        public int BlastResponseId { get; set; }
        public int BlastScheduleId { get; set; }
        public string Response { get; set; }
        public string UserId { get; set; }
        public int StudyId { get; set; }
        public DateTime TimeRead { get; set; }
        public Boolean ReadByNurse { get; set; }
    }
}